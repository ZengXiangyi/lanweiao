# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.find_or_create_by(email: "admin@example.com") do |user|
  user.password = "12345678"
  user.password_confirmation = "12345678"
  puts "created admin user -> email: admin@example.com, password: 12345678"
end

cities = [
    "shanghai",
    "suzhou"
]
cities.each do |name|
  City.create( name: name)
end


location_list = [
    [ "苏州美罗", 120.62848,31.316135,"观前街245号F4层4040	",2 ],
]

location_list.each do |name, latitude, longitude, add, city_id |
  Location.create( location_name: name, latitude: latitude, longitude: longitude,location_add: add, city_id: city_id)
end
