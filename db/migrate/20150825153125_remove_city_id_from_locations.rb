class RemoveCityIdFromLocations < ActiveRecord::Migration
  def change
    remove_column :locations, :city_id
  end
end
