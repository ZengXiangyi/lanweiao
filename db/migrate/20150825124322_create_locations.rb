class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string   "location_name"
      t.string   "location_add"
      t.string   "latitude"
      t.string   "longitude"
      t.integer  "city_id"
      t.string   "zipcode"
      t.string   "email"
    end
  end
end
