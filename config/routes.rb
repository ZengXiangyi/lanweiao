Rails.application.routes.draw do


  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  scope "(:locale)", locale: /en|zh-cn/ do
    devise_for :users
    root 'staticpages#home'
    resources :product_categories do
      resources :products, only: [:index]
    end
    resources :products, only: [:index]
    resources :introductions_product do
      collection do
        get 'fabric'
        get 'technology'
      end
    end
    resources :introductions_company, only: [:index]
    resources :locations, only: [:index]
    resources :services do
      collection do
        get 'group_customization'
        get 'individual_customization'
        get 'oem_odm'
      end
    end
    resources :join_us, only: [:index]
    resources :sale_sites do
      collection do
        get 'contact_us'
        get 'stores'
        get 'sale_network'
      end
    end
  end

  match '*path'                       => redirect('/'),                       via: [:get]
end
