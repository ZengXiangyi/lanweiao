require 'i18n'
I18n.default_locale = :en
RailsAdmin.config do |config|
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)
  config.authorize_with :cancan

  config.main_app_name = ['laiweiao', 'Admin']

  config.model User do
    list do
      field :email do
        label "Email"
      end
      field :created_at
    end
  end

  config.model Product do
    configure :translations, :globalize_tabs
    list do
      field :name
      field :image
    end
  end

  config.model ProductCategory do
    configure :translations, :globalize_tabs
    list do
      field :name
    end
  end

  config.model City do
    #configure :translations, :globalize_tabs
    list do
      field :name
    end
  end

  config.model Location do
    #configure :translations, :globalize_tabs
    list do
      field :location_name
      field :location_add
      field :latitude
      field :longitude
      field :city_id
    end
  end

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.included_models = ['Product','Product::Translation','ProductCategory','ProductCategory::Translation','User','City','Location']
end
