module ApplicationHelper
  def language_name(locale)
    case locale
      when :'en' then "English"
      when :'zh-cn' then "中文"
      else locale
    end
  end
end
