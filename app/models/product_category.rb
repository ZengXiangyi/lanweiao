class ProductCategory < ActiveRecord::Base
  translates :name
  has_many :products
  accepts_nested_attributes_for :translations, allow_destroy: true
end
