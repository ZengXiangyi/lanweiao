class Product < ActiveRecord::Base
  translates :name
  belongs_to :product_category
  accepts_nested_attributes_for :translations, allow_destroy: true

  has_attached_file :image,

      :styles => {  :thumbnail => {:geometry => '560x750#'}},
      :url => ":normalized_image_file_name_:style.:extension",
      :path => ":rails_root/public/:normalized_image_file_name_:style.:extension",
      :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/


  Paperclip.interpolates :normalized_image_file_name do |attachment, style|
    attachment.instance.normalized_image_file_name
  end

  def normalized_image_file_name
    "/web/products/#{self.id}/images/"
  end

end
