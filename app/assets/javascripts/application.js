// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require bootstrap/js/bootstrap.min
//= require bootstrap/js/bootstrap-hover-dropdown.min
//= require owl-carousel/owl.carousel
//= require modernizr.custom
//= require jquery.stellar
//= require imagesloaded.pkgd.min
//= require masonry.pkgd.min
//= require jquery.pricefilter
//= require bxslider/jquery.bxslider.min
//= require mediaelement-and-player
//= require waypoints.min
//= require flexslider/jquery.flexslider-min
//= require theme.plugins
//= require theme
//= require_self
//#= require_tree .


