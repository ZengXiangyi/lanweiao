class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :set_locale
  before_action :get_categories

  def set_locale
    if self.kind_of? RailsAdmin::ApplicationController
      I18n.locale = :en
    else
      I18n.locale = params[:locale] || I18n.default_locale
    end
  end

  def default_url_options(options = {})
    {locale: I18n.locale}
  end

  def get_categories
    @categories = ProductCategory.all
  end
end
